import React, {ReactElement} from 'react';
import {SafeAreaView, ScrollView, Dimensions} from 'react-native';

import {Ionicons, MaterialCommunityIcons} from '@expo/vector-icons';
// @ts-ignore
import bg from '../../utils/bg.jpeg';

import Button from '../uis/Button';
import Picker from '../uis/DropdownPicker';
import Profiles from '../uis/ProfileCards';
import {ProfileType} from '../uis/ProfileCards/Profiles';
import ProgressBar from '../uis/ProgressBar';
import PageIndicator from '../uis/PageIndicator/PageIndicator';
import {
  Heading1,
  Heading2,
  Heading3,
  Body1,
  Body2,
  Body3,
} from '../uis/Typography';
import HeadingWithIcon from '../uis/HeadingWithIcon/HeadingWithIcon';

import {useTheme} from '../../providers/ThemeProvider';

interface Props {}

export default function Test({}: Props): ReactElement {
  const {theme} = useTheme();
  const width = Dimensions.get('window').width * 0.95;

  return (
    <SafeAreaView style={{alignItems: 'center'}}>
      <ScrollView
        contentContainerStyle={{
          alignItems: 'center',
          width,
          justifyContent: 'center',
        }}>
        <HeadingWithIcon
          icon={<Ionicons name="notifications-outline" size={23} />}>
          {'Notifications'}
        </HeadingWithIcon>
        <Button
          text={'Search'}
          withBackground={true}
          bgImage={bg}
          onPress={() => console.log('pressed')}
          icon={<Ionicons name="search" size={20} color={theme.black} />}
          style={{
            borderColor: theme.borderDark,
            borderWidth: 1,
          }}
        />

        <Picker
          items={[
            {label: 'Football', value: 'Football'},
            {label: 'Hockey', value: 'Hockey'},
          ]}
          onValueChange={(value: string | number | boolean) =>
            console.log(value)
          }
        />
        <Profiles
          type={ProfileType.Profile1}
          text="Some text"
          image={bg}
          icon={<MaterialCommunityIcons name="cards-spade" size={40} />}
          style={{width: 300}}
          onPress={() => console.log('card pressed')}
        />
        <Profiles
          type={ProfileType.Profile2}
          image={bg}
          icon={<MaterialCommunityIcons name="cards-spade" size={40} />}
          style={{width: 300, marginVertical: 10}}
          onPress={() => console.log('card pressed')}
        />

        <ProgressBar progress={0.75} style={{marginVertical: 30}} />

        <PageIndicator num={10} active={7} onPress={console.log} />

        <Heading1>Heading 1</Heading1>
        <Heading2>Heading 2</Heading2>
        <Heading3>Heading 3</Heading3>
        <Body1>Body 1</Body1>
        <Body2>Body 1</Body2>
        <Body3>Body 1</Body3>
      </ScrollView>
    </SafeAreaView>
  );
}
