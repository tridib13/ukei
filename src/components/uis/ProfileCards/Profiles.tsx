import React, {ReactElement} from 'react';

import {
  View,
  StyleSheet,
  Text,
  ImageBackground,
  ImageSourcePropType,
  StyleProp,
  ViewStyle,
  TouchableOpacityProps,
  TouchableOpacity,
} from 'react-native';

import {LinearGradient} from 'expo-linear-gradient';
import {useTheme} from '../../../providers/ThemeProvider';

interface Props extends TouchableOpacityProps {
  image: ImageSourcePropType;
  type: number;
  text?: string;
  icon?: ReactElement;
  style?: StyleProp<ViewStyle>;
}

export enum ProfileType {
  Profile1,
  Profile2,
}

const styles = StyleSheet.create({
  image: {
    maxHeight: 400,
    width: '100%',
    borderRadius: 10,
    overflow: 'hidden',
    borderWidth: 1,
  },
  overlay: {
    height: '100%',
    width: '100%',
  },
  upperIcon: {
    position: 'absolute',
    top: 20,
    left: 20,
  },
  lowerIcon: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    transform: [{rotate: '180deg'}],
  },
  centred: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontWeight: '900',
    fontSize: 20,
  },
});

export default function Profile(props: Props): ReactElement {
  const {theme} = useTheme();

  const gradientColors = [
    'rgba(255,255,255,0.8)',
    'rgba(255,255,255,0)',
    'rgba(255,255,255,0.9)',
  ]

  const Profile1 = (
      <LinearGradient
        colors={gradientColors}>
        <View style={styles.upperIcon}>{props.icon}</View>
        <View style={[styles.overlay, styles.centred]}>
          <Text style={styles.text}>{props.text}</Text>
        </View>
        <View style={styles.lowerIcon}>{props.icon}</View>
      </LinearGradient>
    );

  const Profile2 = (
      <LinearGradient
        colors={gradientColors}
        style={styles.overlay}>
        <View style={[styles.upperIcon, styles.centred]}>
          <Text style={styles.text}>{props.text}</Text>
          {props.icon}
        </View>
        <View style={[styles.lowerIcon, styles.centred]}>
          <Text style={styles.text}>{props.text}</Text>
          {props.icon}
        </View>
      </LinearGradient>
    );

  return (
    <TouchableOpacity {...props} activeOpacity={0.6}>
      <ImageBackground
        source={props.image}
        style={[styles.image, props.style, {borderColor: theme.black}]}>
        {props.type === ProfileType.Profile1 ? Profile1 : Profile2}
      </ImageBackground>
    </TouchableOpacity>
  );
}
