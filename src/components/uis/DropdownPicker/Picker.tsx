import React, {ReactElement} from 'react';
import {View, StyleSheet} from 'react-native';
import RNPickerSelect, {PickerSelectProps} from 'react-native-picker-select';
import {useTheme} from '../../../providers/ThemeProvider';

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    padding: 10,
  },
});

// Picker returns null when no option is selected

export default function DropdownPicker(props: PickerSelectProps): ReactElement {
  const {theme} = useTheme();
  return (
    <View style={[styles.container, {borderColor: theme.borderDark}]}>
      <RNPickerSelect {...props} />
    </View>
  );
}
