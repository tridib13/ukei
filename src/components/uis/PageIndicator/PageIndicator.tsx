import React, {ReactElement} from 'react';

import {View, StyleSheet} from 'react-native';

import IndicatorBlock from './IndicatorBlock';

interface Props {
  num: number;
  active: number;
  onPress: (index: number) => any;
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

const PageIndicator = (props: Props): ReactElement => (
  <View style={styles.container}>
    {[...Array(props.num)].map((_, i) => (
      <IndicatorBlock
        key={i}
        checked={props.active === i}
        startBlock={i === 0}
        endBlock={i === props.num - 1}
        onPress={() => props.onPress(i)}
        width={100 / (props.num + 1)}
      />
    ))}
  </View>
);

export default PageIndicator;
