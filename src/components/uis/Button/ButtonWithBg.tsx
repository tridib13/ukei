import React, {ReactElement} from 'react';

import {
  View,
  StyleSheet,
  Text,
  ImageBackground,
  ButtonProps,
  ViewStyle,
  StyleProp,
  ImageSourcePropType,
  TouchableOpacity,
  TouchableOpacityProps,
} from 'react-native';

interface Props extends TouchableOpacityProps {
  icon?: ReactElement;
  children: string;
  image: ImageSourcePropType;
  style?: StyleProp<ViewStyle>;
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    maxHeight: 40,
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  innerContainer: {
    marginHorizontal: 30,
  },
  image: {
    width: '100%',
    borderRadius: 4,
  },
});

export default function ButtonWithBg({
  style,
  ...restOfProps
}: Props): ReactElement {
  return (
    <TouchableOpacity {...restOfProps} style={[styles.image, style]}>
      <ImageBackground source={restOfProps.image} style={{width: '100%'}}>
        <View
          style={[
            styles.container,
            {justifyContent: restOfProps.icon ? 'space-between' : 'center'},
          ]}>
          <View style={restOfProps.icon ? {marginHorizontal: 30} : {}}>
            {restOfProps.icon}
          </View>
          <View style={styles.innerContainer}>
            <Text style={{fontWeight: 'bold'}}>{restOfProps.children}</Text>
          </View>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
}
